@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add Product</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<form action="{{url('/product/create/')}}" role="form" method="post" name="create_article" id="add_form">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="form-group">
							<label for="title">Product name</label>
							<input type="text" class="form-control" name="product_name" id="product_name" placeholder="Product Name" value="{{ old('product_name') }}">
						</div>
						<div class="form-group">
							<label for="description">Quantity in stock</label>
							<input type="text" class="form-control" name="quantity_in_stock" id="quantity_in_stock" placeholder="Quantity in stock" value="{{ old('quantity_in_stock') }}">
						</div>
						<div class="form-group">
							<label for="description">Price per item</label>
							<input type="text" class="form-control" name="price_per_item" id="price_per_item" placeholder="Price per item" value="{{ old('price_per_item') }}">
						</div>
						<button type="button" id="save" class="btn btn-success">Save Product</button>
					</form>
                </div>
           </div>
        </div>
    </div>
    <div id="list_data">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">List Products</div>
					<div class="panel-body">
						<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom:0px !important;">
							<thead>
								<tr>
									<th style="width:20%;">Product name</th>
									<th style="width:20%;">Quantity in stock</th>
									<th style="width:20%;">Price per item</th>
									<th style="width:10%;">Datetime submitted</th>
									<th style="width:10%;">Total value number</th>
								</tr>
							</thead>
							<tbody>
								@if(count($porducts) > 0)
									<?php
										$sum_value_number=0;
									?>
									@foreach($porducts as $product)
										<?php
											$total_value_number= ($product->quantity_in_stock * $product->price_per_item);
											$sum_value_number +=$total_value_number
										?>
										<tr>
											<td style="width:20%;" class="product_name">{{$product->product_name}}</td>
											<td style="width:20%;" class="product_stock">{{$product->quantity_in_stock}}</td>
											<td style="width:20%;" class="product_item">{{$product->price_per_item}}</td>
											<td style="width:10%;">{{$product->date_created}}</td>
											<td style="width:10%;">{{$total_value_number}}</td>
										</tr>
									@endforeach
									<tr>
										<td colspan="4" style="width:20%;">Sum total of all of the Total Value numbers</td>
										<td style="width:20%;">{{$sum_value_number}}</td>
									</tr>
								@else
								<tr>
									<td colspan="6">No record found</td>
								</tr>
								@endif
							</tbody>
						</table>
						
					</div>
			   </div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$('#save').click(function(){
		$.ajax({
			type: 'POST',
			url: "{{ URL::to('/product/create/') }}",
			data: $('#add_form').serialize(),
			success: function(response) {
				$('#add_form')[0].reset();
				$('#list_data').html('');
				$('#list_data').html(response);
			}
		});
	});
	
	
</script>
@endsection
