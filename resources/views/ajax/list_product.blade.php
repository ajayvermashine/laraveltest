<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<div class="panel panel-default">
			<div class="panel-heading">List Products</div>
			<div class="panel-body">
				<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom:0px !important;">
					<thead>
						<tr>
							<th style="width:20%;">Product name</th>
							<th style="width:20%;">Quantity in stock</th>
							<th style="width:20%;">Price per item</th>
							<th style="width:10%;">Datetime submitted</th>
							<th style="width:10%;">Total value number</th>
						</tr>
					</thead>
					<tbody>
						@if(count($porducts) > 0)
							<?php
								$sum_value_number=0;
							?>
							@foreach($porducts as $product)
								<?php
									$total_value_number= ($product->quantity_in_stock * $product->price_per_item);
									$sum_value_number +=$total_value_number
								?>
								<tr>
									<td style="width:20%;">{{$product->product_name}}</td>
									<td style="width:20%;">{{$product->quantity_in_stock}}</td>
									<td style="width:20%;">{{$product->price_per_item}}</td>
									<td style="width:10%;">{{$product->date_created}}</td>
									<td style="width:10%;">{{$total_value_number}}</td>
									

									
								</tr>
							@endforeach
							<tr>
								<td colspan="4" style="width:20%;">Sum total of all of the Total Value numbers</td>
								<td style="width:20%;">{{$sum_value_number}}</td>
							</tr>
						@else
						<tr>
							<td colspan="6">No record found</td>
						</tr>
						@endif
					</tbody>
				</table>
			</div>
	   </div>
	</div>
</div>
