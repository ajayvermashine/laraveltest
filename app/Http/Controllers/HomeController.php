<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$filename= base_path()."/public/assets/product.json";
		$porducts= array();
		try
		{
			$contents = File::get($filename);
			$porducts= json_decode($contents);
		}
		catch (Illuminate\Filesystem\FileNotFoundException $exception)
		{
			$porducts= array();
		}
		//dd($porducts);die;
        return view('welcome', compact('porducts'));
    }
    
    public function create(Request $request)
    {
		$validator = $this->validator($request->all());
		if ($validator->fails()){
			$this->throwValidationException(
				$request, $validator
			);
		}
		$inputs= $request->except('_token');
		$inputs['date_created']= date('m/d/Y H:i:s');
		$filename= base_path()."/public/assets/product.json";
		try
		{
			$contents = File::get($filename);
			$porducts= json_decode($contents);
			if($porducts){
				array_push($porducts, $inputs);
				$new_products= json_encode($porducts);
				File::put($filename, $new_products);
			}else{
				$new_products= [$inputs];
				$new_products= json_encode($new_products);
				File::put($filename, $new_products);
			}
		}
		catch (Illuminate\Filesystem\FileNotFoundException $exception)
		{
			die("The file doesn't exist");
		}
		return $this->listProduct();
		//File::put('path', 'contents');
	}
	
	public function listProduct()
	{
		$filename= base_path()."/public/assets/product.json";
		$porducts= array();
		try
		{
			$contents = File::get($filename);
			$porducts= json_decode($contents);
		}
		catch (Illuminate\Filesystem\FileNotFoundException $exception)
		{
			$porducts= array();
		}
		return view('ajax/list_product', compact('porducts'));
	}
	
	public function validator(array $data)
	{
		return Validator::make($data, [
			'product_name' => 'required|max:255',
			'quantity_in_stock' => 'required|numeric',
			'price_per_item' => 'required|numeric',
		]);
	}
}
